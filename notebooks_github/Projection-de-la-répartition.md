Projection de la répartition sur le corpus complet
================
Pierre-Carl Langlais
28 juin 2021

Ce document exécutable donne une estimation statistique de la
répartition des champs de l’échantillon annoté sur l’ensemble du corpus
des images de l’année 2019. Il peut être également être exécuté sur un
nouvel échantillon sous réserve d’utiliser les mêmes critères
d’annotation.

Pour rappel, l’échantillon a été produit à partir d’une sélection
aléatoire “parfaite” du corpus : à la différence des sondages d’opinion
classique, il n’y a pas eu de biais de sélection. Il est ainsi possible
de donner une estimation directement en utilisant la formule classique
du calcul de la marge d’erreur sans opérer de correction ou de
redressement (comme la méthode des “quotas”).

La même approche pourra être rééditée sur le nombre d’images pertinentes
estimées par le document exécutable “Application des modèles à un
nouveau corpus”. Cette dernière estimation tient déjà compte de la
stratification du corpus (en discipline, en format et par plateforme).
En l’absence d’évolution significative des pratiques, liées par exemple
à l’intégration de la licence d’utilisation des images d’arts visuels
dans les usages de certaines disciplines, l’extrapolation de
l’échantillon annotée devrait rester approximativement exacte.

# 1. Préparation des données.

Nous ouvrons le fichier d’annotation et enregistrons le nombre total
d’images annotées.

``` r
options(scipen=999)
library(tidyverse)

annotation = read_tsv("echantillon_annote.tsv")
total_annotation = nrow(annotation)
```

Nous calculons les proportions en pourcentage (ou plus exactement en
“pour 1”) :

``` r
sum_annotation = annotation %>% 
  count(status_image, name = "image") %>%
  mutate(proportion = (image/sum(image)))

sum_annotation
```

    ## # A tibble: 8 x 3
    ##   status_image                  image proportion
    ##   <chr>                         <int>      <dbl>
    ## 1 Création d'un auteur associé     69     0.0453
    ## 2 Création de l'auteur            296     0.194 
    ## 3 Hors droit d'auteur             124     0.0815
    ## 4 Hors MESRI                      153     0.101 
    ## 5 Hors périmètre                  438     0.288 
    ## 6 Image sous droit                389     0.256 
    ## 7 Licence libre                    32     0.0210
    ## 8 Publication à visée lucrative    21     0.0138

Nous devons calculer à part le corpus restant pondéré en agrégeant les
probabilités de droit d’auteur. Ce calcul tient compte des œuvres
orphelines qui peuvent être datées entre 1890 et 1950 et suit la formule
linéaire :

((*A**n**n**é**e* *d**e* *p**u**b**l**i**c**a**t**i**o**n* \* 0.017) − 32.14) \* 100

![](Projection-de-la-répartition_files/figure-gfm/unnamed-chunk-3-1.png)<!-- -->

``` r
annotation_corpus_pondere = annotation %>%
  filter(status_image == "Image sous droit") %>%
  summarise(image = sum(Probabilite_DA)) %>%
  mutate(proportion = image/total_annotation) %>%
  mutate(status_image = "Image sous droit pondérée")

sum_annotation = sum_annotation %>% bind_rows(annotation_corpus_pondere)

sum_annotation
```

    ## # A tibble: 9 x 3
    ##   status_image                  image proportion
    ##   <chr>                         <dbl>      <dbl>
    ## 1 Création d'un auteur associé    69      0.0453
    ## 2 Création de l'auteur           296      0.194 
    ## 3 Hors droit d'auteur            124      0.0815
    ## 4 Hors MESRI                     153      0.101 
    ## 5 Hors périmètre                 438      0.288 
    ## 6 Image sous droit               389      0.256 
    ## 7 Licence libre                   32      0.0210
    ## 8 Publication à visée lucrative   21      0.0138
    ## 9 Image sous droit pondérée      381.     0.250

# 2. Calcul de la marge d’erreur et des estimations

Nous calculons la marge d’erreur en suivant la formule classique des
sondages sur la base d’un intervalle de confiance à 95% soit :

*z*<sub>0.95</sub> = 1.96
*n* = *t**o**t**a**l* *d**e**s* *a**n**n**o**t**a**t**i**o**n**s*
*P* = *p**r**o**p**o**r**t**i**o**n* *e**n* %

Le calcul à effectuer est donc le suivant :

$$z\_{0.95}\\sqrt{\\frac{\\sigma\_{P}^2}{n}} = 1.96\\sqrt{\\frac{\\sigma\_{P}^2}{1522}}$$

``` r
sum_annotation = sum_annotation %>% 
  mutate(margin_error = 1.96*sqrt((proportion*(1-proportion))/total_annotation)) %>% 
  mutate(proportion = round(proportion*100, 2), margin_error = round(margin_error*100, 2))
```

Nous allons maintenant visualiser les résultats. Avant cela nous
changeons le nom du “statut” des images pour qu’ils soient plus
explicites et définissons un ordre hiérarchique :

``` r
order_status = tibble(status_image = c("Hors périmètre", "Hors droit d'auteur", "Licence libre", "Publication à visée lucrative", "Hors MESRI", "Création de l'auteur", "Création d'un auteur associé", "Image sous droit", "Image sous droit pondérée"), 
                      status_id = 1:9,
                      status_image_label = c("Hors art visuel", "Hors droit d'auteur", "Licence libre", "Publication lucrative", "Hors ESR français", "Création de l'auteur", "Création d'un auteur associé", "Corpus restant", "Corpus restant pondéré"))

sum_annotation = sum_annotation %>%
  inner_join(order_status, by=c("status_image")) 
```

La visualisation donne un aperçu de la valeur minimale et maximale
estimée de chaque image sur la base de l’intervalle de confiance à 95%.
Par exemple pour le corpus restant pondéré la marge d’erreur est de
2.18% : sur la base d’une proportion de 25.01%, l’estimation se trouve
dans une fourchette allant de 22.83% à 27.19%.

``` r
ggplot(sum_annotation, aes(reorder(status_image_label, -status_id), proportion, color = reorder(status_image_label, -status_id))) +
      geom_text(aes(label = paste0(round(proportion-margin_error), "%"), y=(proportion-margin_error)-0.7), angle = 90, color = "black") +
    geom_text(aes(label = paste0(round(proportion+margin_error), "%"), y=(proportion+margin_error)+0.7), angle = 90, color = "black") +
    geom_point() +
    geom_errorbar(aes(ymin = proportion-margin_error, ymax=proportion+margin_error)) +
    guides(color = FALSE) +
    coord_flip() +
    theme_classic() + labs(x="Étape de l'annotation", y="Proportion d'image par discipline")
```

    ## Warning: `guides(<scale> = FALSE)` is deprecated. Please use `guides(<scale> =
    ## "none")` instead.

![](Projection-de-la-répartition_files/figure-gfm/unnamed-chunk-7-1.png)<!-- -->

Nous enregistrons ces estimations.

``` r
write_tsv(sum_annotation, "summary_annotation.tsv")
```

# 3. Extrapolation à l’ensemble du corpus.

Pour l’année 2019, nous avons récolté et catégorisé automatiquement
l’ensemble des images classées comme des représentations figuratives ou
comme des documents. Ces résultats se trouvent dans le fichier
“corpus\_image\_figuratif\_document” :

``` r
corpus_image = read_tsv("corpus_image_figuratif_document.zip")
corpus_image %>% head(100)
```

    ## # A tibble: 100 x 10
    ##    file     link_image     link_document  title type  classification probability
    ##    <chr>    <chr>          <chr>          <chr> <chr> <chr>                <dbl>
    ##  1 1.cxj9m… https://lebru… http://lebrun… Le B… Blog  painting_port…        64.2
    ##  2 1.e7h98… https://lebru… http://lebrun… Le B… Blog  photo_portrait       100. 
    ##  3 1.e7h98… https://lebru… http://lebrun… Le B… Blog  photo_scene           47.7
    ##  4 1.e7h98… https://lebru… http://lebrun… Le B… Blog  drawing               45.1
    ##  5 1.e7h98… https://lebru… http://lebrun… Le B… Blog  drawing               97.8
    ##  6 1.xmkau… https://lebru… http://lebrun… Le B… Blog  drawing               50.7
    ##  7 1.g705a… journals.open… http://journa… L’Or… Revue photo_scene           91.5
    ##  8 1.g705a… journals.open… http://journa… L’Or… Revue photo_portrait        35.4
    ##  9 1.88od1… http://source… http://source… Défe… Blog  photo_landsca…        50.6
    ## 10 1.jjs45… http://source… http://source… Défe… Blog  coins                 99.7
    ## # … with 90 more rows, and 3 more variables: isidore_id <chr>, platform <chr>,
    ## #   type_platform <chr>

Nous avons au total 244589 images. Nous pouvons extrapoler directement
les estimations et les marges d’erreur calculées sur la base de
l’échantillon annoté :

``` r
total_image_corpus = nrow(corpus_image)

sum_annotation = sum_annotation %>%
  mutate(corpus_image = total_image_corpus*(proportion/100)) %>%
  mutate(corpus_margin = total_image_corpus*(margin_error/100))

sum_annotation %>% select(Statut = status_image_label, Estimation = corpus_image, "Marge d'erreur" = corpus_margin)
```

    ## # A tibble: 9 x 3
    ##   Statut                       Estimation `Marge d'erreur`
    ##   <chr>                             <dbl>            <dbl>
    ## 1 Création d'un auteur associé     11080.            2568.
    ## 2 Création de l'auteur             47573.            4867.
    ## 3 Hors droit d'auteur              19934.            3351.
    ## 4 Hors ESR français                24581.            3693.
    ## 5 Hors art visuel                  70393.            5552.
    ## 6 Corpus restant                   62517.            5356.
    ## 7 Licence libre                     5136.            1761.
    ## 8 Publication lucrative             3375.            1443.
    ## 9 Corpus restant pondéré           61172.            5332.

Nous créons la visualisation pour avoir un aperçu d’ensemble des
estimations et des marges d’erreur :

``` r
sum_annotation %>%
  ggplot(aes(reorder(status_image_label, -status_id), corpus_image, color = reorder(status_image_label, -status_id))) +
    geom_point() +
    geom_text(aes(label = round(corpus_image-corpus_margin), y=(corpus_image-corpus_margin)-2000), angle = 90, color = "black") +
    geom_text(aes(label = round(corpus_image+corpus_margin), y=(corpus_image+corpus_margin)+2000), angle = 90, color = "black") +
    geom_errorbar(aes(ymin = corpus_image-corpus_margin, ymax=corpus_image+corpus_margin)) +
    guides(color = FALSE) +
    coord_flip() +
    theme_classic() + labs(x="Étape de l'annotation", y="Proportion d'image par discipline")
```

    ## Warning: `guides(<scale> = FALSE)` is deprecated. Please use `guides(<scale> =
    ## "none")` instead.

![](Projection-de-la-répartition_files/figure-gfm/unnamed-chunk-11-1.png)<!-- -->

La visualisation donne de nouveau un aperçu de la valeur minimale et
maximale estimée de chaque image sur la base de l’intervalle de
confiance à 95%. Ainsi pour le corpus restant la marge d’erreur est de
5332 images : sur la base d’une estimation à 61172 images, la marge
d’erreur se situe dans une fourchette allant de 55840 images à 66504
images.
