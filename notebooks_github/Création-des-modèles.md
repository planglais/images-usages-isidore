Création des modèles prédictifs
================

Ce notebook documente la création d’un modèle d’identification du nombre
d’images figuratives et de reproduction de documents (soit l’ensemble
qui a servi de base à la constitution du corpus annoté)

Cette version simplifiée se dispense de la prédiction des disciplines à
partir du titre. Moyennant une réduction d’information, le script est
considérablement plus rapide.

# 1. Préparation des données

Nous ouvrons maintenant le fichier “composite” comprenant les
métadonnées des publications de l’année 2019 et les métadonnées de Cairn
pour l’année 2017, le fichier des disciplines et le fichier des titres.

``` r
library(tidyverse)
```

    ## ── Attaching packages ─────────────────────────────────────── tidyverse 1.3.1 ──

    ## ✓ ggplot2 3.3.5     ✓ purrr   0.3.4
    ## ✓ tibble  3.1.0     ✓ dplyr   1.0.7
    ## ✓ tidyr   1.1.3     ✓ stringr 1.4.0
    ## ✓ readr   1.4.0     ✓ forcats 0.5.1

    ## ── Conflicts ────────────────────────────────────────── tidyverse_conflicts() ──
    ## x dplyr::filter() masks stats::filter()
    ## x dplyr::lag()    masks stats::lag()

``` r
isidore_metadata = read_tsv("isidore_full_meta.zip")
```

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   isidore_date = col_double(),
    ##   isidore_id = col_character(),
    ##   link = col_character(),
    ##   collection = col_character(),
    ##   title = col_character(),
    ##   type = col_character(),
    ##   description = col_character(),
    ##   home = col_character(),
    ##   publisher_id = col_character(),
    ##   publisher_home = col_character(),
    ##   format = col_character(),
    ##   format_eu_repo = col_character(),
    ##   format_normalized = col_character()
    ## )

``` r
isidore_2019_title = read_tsv("isidore_2019_title.zip")
```

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   isidore_id = col_character(),
    ##   title = col_character()
    ## )

``` r
isidore_2019_disc = read_tsv("isidore_2019_discipline.zip")
```

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   date = col_double(),
    ##   isidore_id = col_character(),
    ##   link = col_character(),
    ##   subject = col_character()
    ## )

``` r
isidore_2017_disc = read_tsv("isidore_2017_discipline.zip")
```

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   date = col_double(),
    ##   isidore_id = col_character(),
    ##   subject = col_character()
    ## )

Nous retirons d’emblée Scielo qui n’a aucune collection en France

``` r
isidore_metadata = isidore_metadata %>% 
  filter(!publisher_id %in% c("SciELO"))
```

Nous prenons maintenant la liste des collections annotées.

``` r
meta_collections = read_tsv("isidore_collections.tsv", col_types = cols(.default = "c"))
```

Lorsque l’un des pays de l’éditeur est français, nous le conservons :

``` r
meta_collections_fr = meta_collections %>% 
  distinct(collection, collection_pays) %>%
  filter(grepl("France|france", collection_pays))

isidore_metadata = isidore_metadata %>%
  inner_join(meta_collections_fr, by=c("collection")) %>%
  mutate(isidore_id = basename(isidore_id))
```

Enfin nous ne conservons que les données qui nous intéressent :
l’identifiant, le format de la publication, le type de collection et le
nom de l’éditeur/plateforme.

``` r
isidore_metadata = isidore_metadata %>% distinct(isidore_id, format = format_normalized, type, publisher_id)
```

Nous simplifions les intitulés des éditeurs.

``` r
list_publisher = tibble(publisher_id = c("OpenEdition", "Centre pour la communication scientifique directe", "Cairn", "ABES"), publisher_simple = c("OpenEdition", "CCSD", "Cairn", "Theses"))

isidore_metadata = isidore_metadata %>%
  left_join(list_publisher, by=c("publisher_id")) %>%
  mutate(publisher_simple = ifelse(is.na(publisher_simple), "Autre plateforme", publisher_simple))
```

Nous ne gardons qu’un intitulé de format pris au hasard.

``` r
isidore_metadata = isidore_metadata %>%
  mutate(format = strsplit(format, ", ")) %>%
  unnest(format) %>%
  group_by(isidore_id) %>%
  sample_n(1) %>%
  ungroup()

isidore_metadata = isidore_metadata %>% mutate(format = ifelse(is.na(format), "Article", format))
```

\#2. Préparation des données de discipline

Nous chargeons d’abord l’ensemble des images classées comme des
représentations figuratives ou comme des documents.

``` r
image_corpus = read_tsv("corpus_image_figuratif_document.zip")
```

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   file = col_character(),
    ##   link_image = col_character(),
    ##   link_document = col_character(),
    ##   title = col_character(),
    ##   type = col_character(),
    ##   classification = col_character(),
    ##   probability = col_double(),
    ##   isidore_id = col_character(),
    ##   platform = col_character(),
    ##   type_platform = col_character()
    ## )

``` r
count_image = image_corpus %>%
  count(isidore_id, platform, type_platform, name = "n_images")
```

Au total, nous avons 30713 documents comptant au moins une image
figurative, soit environ 0% du corpus d’origine. Cette proportion
s’explique par la prévalence de documents courts (en particulier les
billets hypotheses), de documents non illustrés ou illustrés
principalement à partir d’images non figuratives.

Nous ne gardons que les 15 disciplines principales (ce qui permet de
conserver l’essentiel du corpus : le résidu sera classé par le modèle
automatique).

``` r
disc_label = tibble(subject = c("shs.anthro-se", "shs.archeo", "shs.archi", "shs.art", "shs.droit", "shs.edu", "shs.eco", "shs.geo", "shs.gestion", "shs.hist", "shs.info", "shs.langue", "shs.litt", "shs.phil", "shs.psy", "shs.scipo", "shs.socio"),
                    discipline = c("Anthropologie", "Archéologie", "Architecture", "Histoire de l'art", "Droit", "Sciences de l'éducation", "Économie & gestion", "Géographie", "Économie & gestion", "Histoire", "Linguistique", "Info com", "Littératures", "Philosophie", "Psychologie", "Sciences politiques", "Sociologie"))

isidore_disc = bind_rows(isidore_2017_disc, isidore_2019_disc) %>%
  distinct(isidore_id, subject) %>%
  mutate(subject = basename(subject), isidore_id = basename(isidore_id)) %>%
  inner_join(disc_label, by=c("subject")) %>%
  select(-subject)
```

Nous créons le jeu de données avec les disciplines déjà documentées et
ne gardons qu’une discipline au hasard pour les documents
multi-disciplinaires. Par contrecoup nous déduisons le jeu de données
sans discipline documentée (ou dont la seule discipline est trop
spécifique pour être intégrée aux analyses postérieures)

``` r
isidore_data_disc = isidore_metadata %>%
  inner_join(isidore_disc, by=c("isidore_id")) %>%
  group_by(isidore_id) %>%
  sample_n(1) %>%
  ungroup()

isidore_metadata = isidore_metadata %>%
  left_join(isidore_data_disc %>% distinct(isidore_id, discipline), by=c("isidore_id")) %>%
  mutate(discipline = ifelse(is.na(discipline), "[inconnue]", discipline))
```

Maintenant que les métadonnées sont prêtes, nous joignons sur le nombre
d’images:

``` r
isidore_metadata_images = isidore_metadata %>%
  left_join(count_image %>% select(isidore_id, n_images), by=c("isidore_id")) %>%
  mutate(n_images = ifelse(is.na(n_images), 0, n_images))

isidore_metadata_images = isidore_metadata_images %>%
  mutate(format = ifelse(is.na(format), "Article", format)) %>%
  mutate(format = ifelse(format == "Blog", "Page web", format))
```

\#3. Création du modèle prédictif du nombre d’images.

Le modèle va reposer sur une combinaison de facteurs catégoriels : le
format de la publication, sa discipline, le statut du site hébergeant et
l’identité de la plateforme. Nous ajoutons un 0 pour bien spécifier que
par défaut nous nous attendons à avoir 0 images relevant de la mesure
par document.

``` r
modele_nombre_images_simple = glm(n_images ~ 0 + format + publisher_simple + discipline, data = isidore_metadata_images)
```

Nous pouvons maintenant visualiser l’effet des différents facteurs :

``` r
library(broom)
coefficients_nombre_images = tidy(modele_nombre_images_simple)

nom_coefficient = tibble(type_coefficient = c("format", "publisher_simple", "discipline"),
                         type_coefficient_lab = c("Format de publication", "Plateforme", "Discipline"))

coefficients_nombre_images %>%
  mutate(type_coefficient = str_extract(term, "Intercept|format|publisher_simple|discipline")) %>%
  mutate(term = gsub("Intercept|format|publisher_simple|discipline", "", term)) %>%
  inner_join(nom_coefficient, by=c("type_coefficient")) %>%
  mutate(p.value = ifelse(p.value > 0.1, 0.1, p.value)) %>%
  ggplot(aes(term, estimate, color = p.value)) +
  geom_segment(aes(x=term, y=0, xend=term, yend = estimate)) +
  geom_point(size=2) +
  facet_wrap(~ type_coefficient_lab, scale = "free_y", ncol = 1) +
  coord_flip() +
  theme_classic() +
  scale_color_gradient2(low = "green", high = "darkred", mid = "orange", midpoint = 0.05, limit = c(0, 0.1)) +
  labs(x="", y="Effet sur le nombre prédit d'image figurative", color = "Degré d'incertitude\n(Valeur p)")
```

![](Création-des-modèles--version-simple-et-rapide-_files/figure-gfm/unnamed-chunk-13-1.png)<!-- -->
La plupart des paramètres ont un impact non-nul sur les données que nous
cherchons à prédire.

Nous sauvons le modèle.

``` r
save(modele_nombre_images_simple, file = "modele_nombre_images_simple.rda")
```
