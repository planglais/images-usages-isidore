Application des modèles à un nouveau corpus
================
Pierre-Carl Langlais
28 juin 2021

Ce notebook permet d’extrapoler les données recueillies et annotées lors
de l’étude d’usage des œuvres d’arts visuelles à de nouvelles
métadonnées recueillies sur Isidore. La modélisation tiendra notamment
compte de l’évolution en terme de représentation des disciplines ou des
plateformes ce qui rend possible des estimations plus affinées qu’une
simple extrapolation des nombres bruts de résultats sur Isidore.

Le notebook présente ici une démonstration sur la base d’une extraction
des publications de 2020 indexés par Isidore effectuées au début du
moins de juin 2021. En raison de la latence entre la publication du
document et sa diffusion sur certaines plateformes (comme les articles
ou les thèses hébergés sur des archives ouvertes), les chiffres peuvent
être encore sous-estimées.

# 1. Préparation des données

Nous ouvrons les données de 2020. Elles se présentent sous la forme de
plusieurs tableaux qui partagent le même identifiant des documents.

``` r
library(tidyverse)
options(scipen=999)
meta_collections = read_tsv("isidore_collections.tsv", col_types = cols(.default = "c"))
isidore_2020_data = read_tsv("isidore_2020_data.zip")
isidore_2020_format = read_tsv("isidore_2020_format.zip")
isidore_2020_title = read_tsv("isidore_2020_title.zip")
isidore_2020_disc = read_tsv("isidore_2020_discipline.zip")
```

Lorsque l’un des pays de l’éditeur est français, nous le conservons :

``` r
meta_collections_fr = meta_collections %>% 
  distinct(collection, collection_pays, publisher_id, type) %>%
  filter(grepl("France|france", collection_pays))

isidore_2020_data = isidore_2020_data %>%
  inner_join(meta_collections_fr, by=c("collection"))
```

Enfin nous ne conservons que les données qui nous intéressent :
l’identifiant, le format de la publication, le type de collection et le
nom de l’éditeur/plateforme.

``` r
isidore_2020_data = isidore_2020_data %>% 
  left_join(isidore_2020_format, by=c("isidore_id")) %>%
  distinct(isidore_id, format, type, publisher_id)
```

Nous simplifions les intitulés des principales plateformes.

``` r
list_publisher = tibble(publisher_id = c("OpenEdition", "Centre pour la communication scientifique directe", "Cairn", "ABES"), publisher_simple = c("OpenEdition", "CCSD", "Cairn", "Theses"))

isidore_2020_data = isidore_2020_data %>%
  left_join(list_publisher, by=c("publisher_id")) %>%
  mutate(publisher_simple = ifelse(is.na(publisher_simple), "Autre plateforme", publisher_simple))
```

Nous ne gardons qu’un intitulé de format pris au hasard.

``` r
isidore_2020_data = isidore_2020_data %>%
  mutate(format = strsplit(format, ", ")) %>%
  unnest(format) %>%
  group_by(isidore_id) %>%
  sample_n(1) %>%
  ungroup()
```

Quand l’intitulé de format n’existe pas nous prenons juste l’article de
manière générique. Et nous prenons un intitulé plus générique pour les
blogs (la catégorie n’est visiblement utilisée que pour les carnets de
recherche d’hypothèses, ce qui n’est pas très apprécié par notre
modèle…)

``` r
isidore_2020_data = isidore_2020_data %>% 
  mutate(format = ifelse(is.na(format), "Article", format)) %>%
  mutate(format = ifelse(format == "Blog", "Page web", format))
```

Nous mettons maintenant en forme les données de discipline. Nous gardons
que les 15 disciplines principales (ce qui permet de conserver
l’essentiel du corpus).

``` r
disc_label = tibble(subject = c("shs.anthro-se", "shs.archeo", "shs.archi", "shs.art", "shs.droit", "shs.edu", "shs.eco", "shs.geo", "shs.gestion", "shs.hist", "shs.info", "shs.langue", "shs.litt", "shs.phil", "shs.psy", "shs.scipo", "shs.socio"),
                    discipline = c("Anthropologie", "Archéologie", "Architecture", "Histoire de l'art", "Droit", "Sciences de l'éducation", "Économie & gestion", "Géographie", "Économie & gestion", "Histoire", "Linguistique", "Info com", "Littératures", "Philosophie", "Psychologie", "Sciences politiques", "Sociologie"))

isidore_disc = isidore_2020_disc %>%
  distinct(isidore_id, subject) %>%
  mutate(subject = basename(subject)) %>%
  inner_join(disc_label, by=c("subject")) %>%
  select(-subject)
```

Nous créons le jeu de données avec les disciplines déjà documentées et
ne gardons qu’une discipline au hasard pour les documents
multi-disciplinaires. Par contrecoup nous déduisons le jeu de données
sans discipline documentée (ou dont la seule discipline est trop
spécifique pour être intégrée aux analyses postérieures)

``` r
isidore_data_disc = isidore_2020_data %>%
  inner_join(isidore_disc, by=c("isidore_id")) %>%
  group_by(isidore_id) %>%
  sample_n(1) %>%
  ungroup()

isidore_2020_data = isidore_2020_data %>%
  left_join(isidore_data_disc %>% distinct(isidore_id, discipline), by=c("isidore_id")) %>%
  mutate(discipline = ifelse(is.na(discipline), "[inconnue]", discipline))
```

# 2. Application du modèle prédictif

Maintenant que les métadonnées sont prêtes, nous chargeons le modèle de
prédiction du nombre d’images :

``` r
load("modele_nombre_images_simple.rda")
```

Le modèle va reposer sur une combinaison de facteurs catégoriels : le
format de la publication, sa discipline, le statut du site hébergeant et
l’identité de la plateforme. Pour rappel, les coefficients du modèle se
répartissaient ainsi :

``` r
library(broom)
coefficients_nombre_images = tidy(modele_nombre_images_simple)

nom_coefficient = tibble(type_coefficient = c("format", "publisher_simple", "discipline"),
                         type_coefficient_lab = c("Format de publication", "Plateforme", "Discipline"))

coefficients_nombre_images %>%
  mutate(type_coefficient = str_extract(term, "Intercept|format|publisher_simple|discipline")) %>%
  mutate(term = gsub("Intercept|format|publisher_simple|discipline", "", term)) %>%
  inner_join(nom_coefficient, by=c("type_coefficient")) %>%
  mutate(p.value = ifelse(p.value > 0.1, 0.1, p.value)) %>%
  ggplot(aes(term, estimate, color = p.value)) +
  geom_segment(aes(x=term, y=0, xend=term, yend = estimate)) +
  geom_point(size=2) +
  facet_wrap(~ type_coefficient_lab, scale = "free_y", ncol = 1) +
  coord_flip() +
  theme_classic() +
  scale_color_gradient2(low = "green", high = "darkred", mid = "orange", midpoint = 0.05, limit = c(0, 0.1)) +
  labs(x="", y="Effet sur le nombre prédit d'image figurative", color = "Degré d'incertitude\n(Valeur p)")
```

![](Application-des-modèles-à-un-nouveau-corpus_files/figure-gfm/unnamed-chunk-10-1.png)<!-- -->

Nous appliquons le modèle aux nouvelles données :

``` r
resultat_modele = predict(modele_nombre_images_simple, isidore_2020_data, type = "response", se.fit = TRUE)
```

Nous intégrons les résultats dans le jeu de données :

``` r
isidore_2020_images = isidore_2020_data %>% mutate(nombre_images = resultat_modele$fit, marge_erreur = resultat_modele$se.fit)
```

À noter que certaines prédictions individuelles du nombre d’image sont
négatives ce qui est impossible en pratique mais permet d’équilibrer les
prédictions globales. Nous pouvons maintenant obtenir l’estimation
générale du nombre d’images :

``` r
paste0("Nous estimons le nombre d'images pertinentes à ", round(sum(isidore_2020_images$nombre_images)))
```

    ## [1] "Nous estimons le nombre d'images pertinentes à 198810"

La marge d’erreur nous permet de déduire une estimation minimale et
maximale du nombre d’images :

``` r
isidore_2020_images = isidore_2020_images %>% mutate(max_images = nombre_images + marge_erreur, min_images = nombre_images - marge_erreur)

paste0("Nous estimons le nombre minimal d'images pertinentes à ", round(sum(isidore_2020_images$min_images)))
```

    ## [1] "Nous estimons le nombre minimal d'images pertinentes à 171613"

``` r
paste0("Nous estimons le nombre maximal d'images pertinentes à ", round(sum(isidore_2020_images$max_images)))
```

    ## [1] "Nous estimons le nombre maximal d'images pertinentes à 226007"

L’incertitude et la marge d’erreur peut également être représentée sous
la forme d’une courbe de probabilité. Nous allons tirer 1000
échantillons de 1000 images au hasard dans notre corpus. La courbe sera
centrée autour notre estimation globale à 198810 images et va se
raréfier au niveau des deux extrêmes de notre intervalle de confiance
(soit entre 171613 images et 226007 images).

Il est possible d’obtenir une courbe plus lisse et plus précise en
tirant davantage d’échantillons t en remplaçant la valeur draw\_sample
par un chiffre plus élevé (mais cela prend plus de temps)

``` r
number_samples = 1000
draw_sample = 1000

ratio_draw_population = nrow(isidore_2020_images)/draw_sample

total_sample = NULL

for(n_sample in 1:number_samples) {
  current_sample = isidore_2020_images %>% select(nombre_images, marge_erreur) %>% sample_n(draw_sample) %>% mutate(sample_id = n_sample)
  
  total_sample = total_sample %>% bind_rows(current_sample)
}

total_sample = total_sample %>%
  group_by(sample_id) %>%
  summarise(nombre_images = sum(nombre_images)) %>%
  mutate(extrapolated_images = nombre_images*ratio_draw_population) %>%
  arrange(extrapolated_images)
```

Nous visualisons maintenant la courbe d’incertitude :

``` r
mean_estimation = total_sample$extrapolated_images[500]

total_sample %>%
  ggplot(aes(extrapolated_images)) +
  geom_density(fill = "red") +
  geom_vline(xintercept = mean_estimation, linetype = "dashed", color = "blue") +
  theme_classic() +
  labs(x="Nombre d'image estimée", y="Courbe de densité d'image")
```

![](Application-des-modèles-à-un-nouveau-corpus_files/figure-gfm/unnamed-chunk-16-1.png)<!-- -->

Nous pouvons de nouveau récupérer les bornes de notre intervalle de
confiance en prenant la 25e et la 975e valeur (soit de 2,5% à 97,5% du
corpus) :

``` r
total_sample = total_sample %>% arrange(extrapolated_images)

intervalle_confiance_bas = total_sample$extrapolated_images[25]
intervalle_confiance_centre = total_sample$extrapolated_images[500]
intervalle_confiance_haut = total_sample$extrapolated_images[975]

print(paste0("Avec ces tirages, l'intervalle de confiance est centré sur ", round(intervalle_confiance_centre, 0), " et va de ", round(intervalle_confiance_bas, 0), " à ", round(intervalle_confiance_haut, 0)))
```

    ## [1] "Avec ces tirages, l'intervalle de confiance est centré sur 198902 et va de 170997 à 228070"

# 3. Application de la distribution statistique de l’échantillon annotée.

Enfin, nous allons extrapoler la distribution statistique de notre
échantillon annoté sur la base de notre estimation de 198810 images.

Nous ouvrons les données agrégées de l’échantillon

``` r
summary_annotation = read_tsv("summary_annotation.tsv")
```

Nous pouvons extrapoler directement les estimations et les marges
d’erreur calculées sur la base de l’échantillon annoté :

``` r
total_image_corpus = round(sum(isidore_2020_images$nombre_images))

sum_annotation = summary_annotation %>%
  mutate(corpus_image = total_image_corpus*(proportion/100)) %>%
  mutate(corpus_margin = total_image_corpus*(margin_error/100))

sum_annotation %>% select(Statut = status_image_label, Estimation = corpus_image, "Marge d'erreur" = corpus_margin)
```

    ## # A tibble: 9 x 3
    ##   Statut                       Estimation `Marge d'erreur`
    ##   <chr>                             <dbl>            <dbl>
    ## 1 Création d'un auteur associé      9006.            2088.
    ## 2 Création de l'auteur             38669.            3956.
    ## 3 Hors droit d'auteur              16203.            2724.
    ## 4 Hors ESR français                19980.            3002.
    ## 5 Hors art visuel                  57218.            4513.
    ## 6 Corpus restant                   50816.            4354.
    ## 7 Licence libre                     4175.            1431.
    ## 8 Publication lucrative             2744.            1173.
    ## 9 Corpus restant pondéré           49722.            4334.

Nous créons la visualisation pour avoir un aperçu d’ensemble des
estimations et des marges d’erreur :

``` r
sum_annotation %>%
  ggplot(aes(reorder(status_image_label, -status_id), corpus_image, color = reorder(status_image_label, -status_id))) +
    geom_point() +
    geom_text(aes(label = round(corpus_image-corpus_margin), y=(corpus_image-corpus_margin)-2000), angle = 90, color = "black") +
    geom_text(aes(label = round(corpus_image+corpus_margin), y=(corpus_image+corpus_margin)+2000), angle = 90, color = "black") +
    geom_errorbar(aes(ymin = corpus_image-corpus_margin, ymax=corpus_image+corpus_margin)) +
    guides(color = FALSE) +
    coord_flip() +
    theme_classic() + labs(x="Étape de l'annotation", y="Proportion d'image par discipline")
```

    ## Warning: `guides(<scale> = FALSE)` is deprecated. Please use `guides(<scale> =
    ## "none")` instead.

![](Application-des-modèles-à-un-nouveau-corpus_files/figure-gfm/unnamed-chunk-20-1.png)<!-- -->

La visualisation donne de nouveau un aperçu de la valeur minimale et
maximale estimée de chaque image sur la base de l’intervalle de
confiance à 95%. Ainsi pour le corpus restant pondéré la marge d’erreur
est de 4334 images : sur la base d’une estimation à 49722 images, la
marge d’erreur se situe dans une fourchette allant de 45388 images à
54056 images.
