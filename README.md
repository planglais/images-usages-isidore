# Images Usages Isidore

Cette collection de carnets de code en R permet de calculer une estimation du nombre total d'illustrations publiés dans les publications scientifiques françaises à partir des métadonnées du moteur de recherche spécialisé Isidore. Elle a été originellement créé pour le projet Usages Images réalisé par Datactivist et Pierre-Carl Langlais pour le Ministère de la recherche et le Ministère de la culture.

L'estimation peut être produite à partir de trois carnets de code : récupération du corpus (qui permet de charger de nouvelles métadonnées d'Isidore), application des modèles à un nouveau corpus (qui produit une estimation du nombre d'images à partir d'une modélisation pondérée du nombre de documents) et projection de la répartition (qui généralise les résultats d'une enquête statistique manuelle sur le statut légal des images). Le carnet de code "Création du modèle" documente la création du modèle de prédiction du nombre d'images. Il n'est pas nécessaire de les utiliser sauf pour modifier les paramètres des modèles ou pour mieux comprendre les choix méthodologiques effectués en amont.

Chaque carnet de code s'ouvre de préférence dans R Studio. Ils peuvent être utilisés sans connaissance préalable du langage R : il suffit d'exécuter successivement chaque cellule de code en cliquant sur le bouton "run" et modifier au besoin certaines variables signalées dans le texte.

# Récupération de corpus

Le carnet "Récupération du corpus" récupère l'ensemble du corpus d'Isidore sur une année. Il doit être impérativement exécuté préalablement aux autres analyses. Pour ne pas affecter le bon fonctionnement d'Isidore, la récupération des données est assez longue. Il est préférable de lancer ce carnet de code d'une traite (en cliquant sur Run => Run All) puis de laisser tourner R pendant environ une dizaine d'heures.

# Application des modèles à un nouveau corpus

Ce carnet permet d'extrapoler les données recueillies et annotées lors de l'étude Usage Images à de nouvelles métadonnées recueillies sur Isidore.

La modélisation tiendra notamment compte de l'évolution en terme de représentation des disciplines ou des plateformes ce qui rend possible des estimations plus affinées qu'une simple extrapolation des nombres bruts de résultats sur Isidore.

# Projection de la répartition

Ce carnet donne une estimation statistique de la répartition des champs de l'échantillon annoté sur l'ensemble du corpus des images de l'année 2019. L'échantillon a été produit à partir d'une sélection aléatoire "parfaite" du corpus : à la différence des sondages d'opinion classique, il n'y a pas eu de biais de sélection. Il est ainsi possible de donner une estimation directement en utilisant la formule classique du calcul de la marge d'erreur sans opérer de correction ou de redressement (comme la méthode des "quotas").

Cette répartition peut être directement appliquée au nombre d'images pertinentes estimées par le carnet précédent "Application des modèles à un nouveau corpus". Cette dernière estimation tient déjà compte de la stratification du corpus (en discipline, en format et par plateforme). En l'absence d'évolution significative des pratiques, liées par exemple à l'intégration de la licence d'utilisation des images d'arts visuels dans les usages de certaines disciplines, l'extrapolation de l'échantillon annotée devrait rester approximativement exacte.
